package com.jeff.mykoin

import android.app.Application
import com.jeff.mykoin.di.factoryModule
import com.jeff.mykoin.di.singleModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class MyApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidLogger()
            androidContext(this@MyApplication)

            modules(listOf(factoryModule, singleModule))
        }
    }
}