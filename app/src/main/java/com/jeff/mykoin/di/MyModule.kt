package com.jeff.mykoin.di

import org.koin.dsl.module

interface Wood

interface Water

interface Ink

interface Paper

interface Printer

class MyWood : Wood

class MyWater : Water

class MyInk(val water: Water) : Ink

class MyPaper(val wood: Wood) : Paper

class MyPrinter(val paper: Paper, val ink: Ink) : Printer

val factoryModule = module {

    factory { MyPrinter(get(), get()) }
}

val singleModule = module {

    single { MyWood() as Wood }

    single { MyWater() as Water }

    single { MyInk(get()) as Ink }

    single { MyPaper(get()) as Paper }
}