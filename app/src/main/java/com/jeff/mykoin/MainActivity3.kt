package com.jeff.mykoin

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.log.JFLog
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity3 : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        MainActivity.count++

        tv_only.text = this::class.simpleName
    }

    override fun onResume() {
        super.onResume()

        JFLog.d("Count: ${MainActivity.count}")
    }
}
