package com.jeff.mykoin

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.log.JFLog
import kotlinx.android.synthetic.main.activity_main.*

// https://www.jianshu.com/p/54d2983bea2c

/**
 * 不同 Process, 資料不共用, Process 在 AndroidManifest 設定, MainActivity 一個 Process, MainActivity2 & 3 一個 Process.
 * launch mode: singleInstance, singleTask, singleTop
 */

class MainActivity2 : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        MainActivity.count++

        tv_only.text = this::class.simpleName
    }

    override fun onResume() {
        super.onResume()

        JFLog.d("Count: ${MainActivity.count}")
    }
}
