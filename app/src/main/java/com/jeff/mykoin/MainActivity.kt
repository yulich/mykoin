package com.jeff.mykoin

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.jeff.mykoin.di.Ink
import com.jeff.mykoin.di.MyPrinter
import com.log.JFLog
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.android.ext.android.inject

class MainActivity : AppCompatActivity() {

    val printer1: MyPrinter by inject()
    val printer2: MyPrinter by inject()
    val ink1: Ink by inject()
    val ink2: Ink by inject()

    companion object {
        var count = 0
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        count++

        tv_only.text = this::class.simpleName

        JFLog.d("Factory")
        JFLog.w("$printer1")
        JFLog.w("$printer2")

        JFLog.d("Singleton")
        JFLog.w("$ink1")
        JFLog.w("$ink2")
        JFLog.w("${printer1.ink}")
        JFLog.w("${printer2.ink}")
    }

    override fun onResume() {
        super.onResume()

        JFLog.d("Count: $count")
    }
}
